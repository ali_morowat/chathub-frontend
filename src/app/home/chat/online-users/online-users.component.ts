import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Observable, forkJoin } from 'rxjs';

@Component({
  selector: 'app-online-users',
  templateUrl: './online-users.component.html',
  styleUrls: ['./online-users.component.css']
})
export class OnlineUsersComponent implements OnInit {
  allUsers: string[];
  onlineUsers: string[];
  allUserAvailabilityAndName;
  
  constructor(public userOnlineService: UserService, private toastr: ToastrService) {}

  ngOnInit() {
    
    setInterval(() => {
      this.userOnlineService.getUserNameAndAvailability().subscribe(
        res => {
          this.allUserAvailabilityAndName = res; 
          this.allUsers = this.allUserAvailabilityAndName;
        },
        err => {
          console.log(err);
        } 
      );
    },1000); 
  }
}
