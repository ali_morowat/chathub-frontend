import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { NgForm } from '@angular/forms';
import { HubConnection,HubConnectionBuilder } from '@aspnet/signalr';
import * as signalR from '@aspnet/signalr';

@Component({
  selector: 'app-home-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class chatComponent implements OnInit {
  userDetails;
  public hubConnection: HubConnection;
 // User should be set here
 formModel = {
   User : '',
   Message : '',
   
  }
  
  messageList: string [] = [];

  constructor(private service: UserService) { }

  ngOnInit() {
    this.formModel.User = this.userDetails;
    
    this.service.getUserProfile().subscribe(
      res => {
        this.userDetails = res;
        this.formModel.User = this.userDetails.userName;
      },
      err => {
        console.log(err);
      }
    );
    
    // HubConnection Process
    this.hubConnection = new HubConnectionBuilder()
      .withUrl('ws://localhost:7000/gateway/', {
        //localhost:7001/chatHub
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
      })
      .build();
   
    this.hubConnection
    .start()
    .then(() => console.log ('Connection Started'))
    .catch(err => console.log('Error while establishing connection'));
    
    this.hubConnection.on('SendMessage', (user : string, receiveMessage: string) => {
      const text = ` ${user}: ${receiveMessage}`;
      this.messageList.push(text);
    });
    // für logout gibt es off or stop hub function in hub libary 

  }

  onSendMessage(form: NgForm): void{
    this.hubConnection
    .invoke('SendMessage', this.formModel.User, this.formModel.Message)
    .then(() => this.formModel.Message = '')
    .catch(err => console.log(err));
  }
}




