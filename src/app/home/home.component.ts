import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../shared/services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  LogUsers; 
  User: '';
  showActiveUser;
  data: any[];
  userRoles: any[];
  isAdmin;

  constructor(private router: Router, private toastr: ToastrService, private userService: UserService) {}

  ngOnInit() {
    this.userService.getUserProfile().subscribe(
      res => {
       /*  console.log(res); */
        this.LogUsers = res;
        this.User     = this.LogUsers.userName;
        this.findUserRole(this.User);
      },
      err => {
        console.log(err);
      }
    );
    this.showActiveUser = false;
    
  }
  
  onLogout() {
  this.userService.deleteToken().subscribe (
      res => {
        sessionStorage.removeItem('token');
        this.router.navigate(['/user/login']);
        this.toastr.info("You have logged out!","CHAT HUB");
      },
      err => {
        console.log(err);
      }
    );
  }

  showOnlinUser(){
    this.showActiveUser = !this.showActiveUser;
  }

  findUserRole(userName){
    this.userService.getUserRole(userName).subscribe(
      (roles: any[]) => {
        this.userRoles = roles;
        for(var u = 0; u <= this.userRoles.length -1; u++){
          if(this.userRoles[u] == "Admin"){
            /* console.log("Hi " + this.userRoles[u]); */
            this.isAdmin = true;
          }
        }
      },
      err => {console.log(Error);}
    );
  }
}
