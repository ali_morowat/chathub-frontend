import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserService } from './shared/services/user.service';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { chatComponent } from './home/chat/chat.component';
import { OnlineUsersComponent } from './home/chat/online-users/online-users.component';
import { ForbiddenComponent } from './home/forbidden/forbidden.component';
import { AdminPanelComponent } from './home/admin-panel/admin-panel.component';
import { DevExtremeModuleModule } from './shared/models/dev-extreme-module/dev-extreme-module.module';
import { VideoComponent } from './home/video/video.component';
import { NewHomeComponent } from './home/new-home/new-home.component';
import { UserManagementComponent } from './user-management/user-management.component';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    chatComponent,
    OnlineUsersComponent,
    AdminPanelComponent,
    ForbiddenComponent,
    VideoComponent,
    NewHomeComponent,
    UserManagementComponent,
   
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    DevExtremeModuleModule
    
  ],
  providers: [UserService],
  bootstrap: [AppComponent],

})
export class AppModule { }
