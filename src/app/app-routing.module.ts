import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { LoginComponent } from './user/login/login.component'
import { HomeComponent } from './home/home.component';
import { SecureGuard } from './shared/guards/secure.guard';
import { AdminPanelComponent } from './home/admin-panel/admin-panel.component';
import { ForbiddenComponent } from './home/forbidden/forbidden.component';
import { VideoComponent } from './home/video/video.component';
import { NewHomeComponent } from './home/new-home/new-home.component';
import { UserManagementComponent } from './user-management/user-management.component';

const routes: Routes = [
  /*Default route*/
  { path: '', redirectTo: '/user/login', pathMatch: 'full' },
  /*Cutomize routze*/
  {
    path: 'user', component: UserComponent,
    children: [
      { path: 'registration', component: RegistrationComponent },
      { path: 'login',        component: LoginComponent        }
    ]
  },
  { 
    path: 'home', 
    component: HomeComponent, 
    canActivate: [
      SecureGuard
    ] 
  },
  { 
    path: 'adminpanel', 
    component: AdminPanelComponent, 
    canActivate: [
      SecureGuard
    ] , data :{permitttedRoles:['Admin']}
  },
  { 
    path: 'forbidden', 
    component: ForbiddenComponent, 
  },
  { 
    path: 'Video', 
    component: VideoComponent, 
  },
  { 
    path: 'NewHome', 
    component: NewHomeComponent, 
  },
  { 
    path: 'UserManagement', 
    component: UserManagementComponent, 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
