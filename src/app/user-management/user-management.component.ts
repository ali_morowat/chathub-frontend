import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserDatas } from '../shared/models/user-datas';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  title = 'User Information'; 
  constructor(private userService: UserService) { }
  data: any;  
  UserForm: FormGroup;  
  submitted = false;   
  EventValue: any = "Save";

  ngOnInit() {
    this.getdata(); 
    this.UserForm = new FormGroup({
      id: new FormControl(null),  
      userName: new FormControl("",[Validators.required]),
      password: new FormControl("",[Validators.required]),        
      email: new FormControl("",[Validators.required]),  
      phoneNumber:new FormControl("",[Validators.required]),  
    })
  }

  getdata() {  
    this.userService.getUserDatas().subscribe((data: any[]) => {  
      this.data = data;  
      console.log(this.data);
    })  
  } 
  
  deleteData(id) {  
    this.userService.deleteUserData(id).subscribe((data: any[]) => {  
      this.data = data;  
      this.getdata();  
    })  
  } 

  Save() {   
    this.submitted = true;  
    
     if (this.UserForm.invalid) {  
            return;  
     }  
    this.userService.postUserData(this.UserForm.value).subscribe((data: any[]) => {  
      this.data = data;  
      this.resetFrom();  
  
    })  
  }
  
  Update() {   
    this.submitted = true;  
    
    if (this.UserForm.invalid) {  
     return;  
    }        
    this.userService.putUserData(this.UserForm.value.id,this.UserForm.value).subscribe((data: any[]) => {  
      this.data = data;  
      this.resetFrom();  
    })  
  }

  EditData(Data) {  
    this.UserForm.controls["id"].setValue(Data.id);  
    this.UserForm.controls["userName"].setValue(Data.userName);  
    this.UserForm.controls["password"].setValue(Data.password);      
    this.UserForm.controls["phoneNumber"].setValue(Data.phoneNumber);  
    this.UserForm.controls["email"].setValue(Data.email);  
    this.EventValue = "Update";  
  } 

  resetFrom()  
    {     
      this.getdata();  
      this.UserForm.reset();  
      this.EventValue = "Save";  
      this.submitted = false;   
    }

}
