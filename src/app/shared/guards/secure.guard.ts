import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class SecureGuard implements CanActivate {
  
  constructor(private router: Router, private _toaster: ToastrService, private userservice: UserService) { }
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (sessionStorage.getItem('token') !== null) {
      let roles = next.data['permitttedRoles'] as Array<string>;
      if(roles){
        if(this.userservice.roleMatch(roles))
          return true;
        else {
          this.router.navigate(['forbidden']);
            return false;
        }
      }
      
      return true;
    }
    else
      this.router.navigate(['user/login']);
    return false;
  }

}
