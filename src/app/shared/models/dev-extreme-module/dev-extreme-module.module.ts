import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DevExtremeModule } from "devextreme-angular";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DevExtremeModule
  ],
  exports: [
    DevExtremeModule
  ]
})
export class DevExtremeModuleModule {
}
