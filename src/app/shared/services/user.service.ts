import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import * as jwt_decode from "jwt-decode";
import { UserNames } from '../models/user.model';
import { OnlineUsers } from '../models/onlineUsers.module';
import { match } from 'minimatch';
import { UserDatas } from '../models/user-datas';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly Url = "http://localhost:7000/chathub-backend-api";
  FormData: UserNames;

  readonly UserManagementUrl = "http://localhost:7000/usermanagement-api";
  UmFormData: UserDatas;


  
  constructor(private fb:FormBuilder, private Http:HttpClient) { }
  UserNameList   : UserNames[];
  OnlineUserList : OnlineUsers [];

 
  
  register(formModel){

    var body = {
     UserName: formModel.value.UserName,
     Email: formModel.value.Email,
     FullName: formModel.value.FullName,
     Password: formModel.value.Passwords.Password,
     IsAdmin: formModel.value.Role
    }
      console.log(body.IsAdmin);
      
    return this.Http.post(this.Url+'/ApplicationUser/Register', body);
  }
  // We are expecting a string here. not json !
  logIn(formData){
    return this.Http.post(this.Url+'/ApplicationUser/Login', formData, { responseType: 'text' });
  }
  getUserRole(userName){
    return this.Http.get(this.Url+'/ApplicationUser/UserAssignedRole/'+ userName);
  }
  getUserProfile(){
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+ sessionStorage.getItem('token')});
      return this.Http.get(this.Url+'/UserProfile',{headers : tokenHeader });
  }
  // Logout : Delete User with the Token from the Table 
  deleteToken(){
    var token = sessionStorage.getItem('token');
    return this.Http.delete(this.Url+'/ApplicationUser/online/' + token);
  }
  getUserNameAndAvailability(){
    return this.Http.get(this.Url+'/ApplicationUser/users/status');
  }
  getAllRoles(){
    return this.Http.get<string[]>(this.Url+'/ApplicationUser/UserRole');
  }
  setRole(){
    var roleName = "Admin";
    return this.Http.post(this.Url+'/ApplicationUser/UserRole', roleName);
  }
  roleMatch(allowedRoles): boolean {
    var isMatch  = false;
    
    var payload  = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    console.log(payload);
    var userRole = payload.role;
    allowedRoles.foreach(element => {
      if( userRole == element ){
        isMatch = true;
        return false;
      }
    }); 
    return isMatch;
  }
  /* UserManagement Data */
  getUserDatas(){
    return this.Http.get(this.UserManagementUrl+'/AspNetUsers/Users');
  }

  postUserData(umFormData){  
    return this.Http.post(this.UserManagementUrl+'/AspNetUsers/',umFormData);  
  } 

  putUserData(id,umFormData){  
    return this.Http.put(this.UserManagementUrl+'/AspNetUsers/'+id,umFormData);  
  }

  deleteUserData(id){  
    return this.Http.delete(this.UserManagementUrl+'/AspNetUsers/'+id);  
  } 



  
}
