import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UserService } from '../../shared/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { RoleList } from 'src/app/shared/models/RoleList';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {

  roleList: RoleList[] = [];

  roles;
  IsRoleSelected = false;

  ListOfCheckBoxValue = [];


  constructor(public service: UserService, private toastr: ToastrService, private fb: FormBuilder) {

  }

  formModel = this.fb.group({
    UserName: ['', Validators.required],
    Email: ['', Validators.email],
    FullName: [''],
    Passwords: this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required]
    },
      { validators: this.comparePasswords }),


    // Role: call the function : give an array of checkboy value 
    IsAdmin: this.ListOfCheckBoxValue

  });



  ngOnInit() {
    this.service.getAllRoles().subscribe(
      res => {
        //this.roles = res;
        Object.assign(this.roleList, res);
        //this.roleList = res;
        //this.roleList = this.roles; // this.roles;
        console.log(this.roleList);

      },
      err => {
        console.log(err);
      }
    );
    this.formModel.reset();

  }

  // retrieve checkbox value "Admin or User"

  onCheckChange(event) {
    // this.ListOfCheckBoxValue = [];
    if (event.target.checked) {
      this.ListOfCheckBoxValue.push(event.target.value);
      console.log(this.ListOfCheckBoxValue);
    }
  }


  comparePasswords(fb: FormGroup) {

    let ConfirmPswrdCtrl = fb.get('ConfirmPassword');
    if (ConfirmPswrdCtrl.errors == null || 'Passwordmismatch' in ConfirmPswrdCtrl.errors) {
      if (fb.get('ConfirmPassword').value == fb.get('Password').value) {
        ConfirmPswrdCtrl.setErrors(null);
      }
      else {
        ConfirmPswrdCtrl.setErrors({ Passwordmismatch: true });
      }
    }
  }

  onSubmit() {

    console.log(this.formModel.value);


    this.service.register(this.formModel).subscribe(

      (res: any) => {
        if (res.succeeded) {
          this.formModel.reset();
          this.toastr.success('new user created', 'registration successfull');
        }
        else {
          res.errors.forEach(element => {
            switch (element.code) {
              case 'DuplicateUserName':
                this.toastr.error('User is already taken', 'duplicate name');
                break;
              default:
                this.toastr.error(element.description, 'error');
                break;
            };
          })
        }
      },
      err => {
        console.log(err);
      }
    );

  }

  giveRole() {
    this.IsRoleSelected = !this.IsRoleSelected;
    return
  }
}
