import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public service: UserService, private toastr: ToastrService, private router: Router) { }
  
  formModel = {
    UserName: '',
    Password: ''
  }

  ngOnInit() {
    // If a user is already logged
    if(sessionStorage.getItem('token') != null)
    this.router.navigateByUrl('/home');
    
  }

  onLogin(form: NgForm) {
    this.service.logIn(form.value).subscribe(
      (res: string) => {
        sessionStorage.setItem('token', res);
        this.router.navigateByUrl('/home');
        this.toastr.success(this.formModel.UserName, "Hi");
      },
      err => {
        // err.status == 400 ? this.toastr.error("Username or Password is incorrect", "Login Failed") : console.log(Error);
        if (err.status == 400)
          this.toastr.error("Username or Password is incorrect", "Login Failed");
        else
          console.log(Error);
      }
    )
  }
}
